package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import org.springframework.stereotype.Service;


public class VirementMapper {

    private static VirementDto virementDto;
    private static Virement virmenet;

    public static VirementDto VirementMap(Virement virement) {
        virementDto = new VirementDto();
        virementDto.setMontant(virement.getMontant());
        virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
        virementDto.setDate(virement.getDateExecution());
        virementDto.setMotif(virement.getMotif());
        return virementDto;

    }
    public static Virement VirementDtoMap(VirementDto virementDto, Compte CompteEmetteur, Compte CompteBeneficiaire ) {
        virmenet= new Virement();
        virmenet.setMontant(virementDto.getMontant());
        virmenet.setMotif(virementDto.getMotif());
        virmenet.setCompteEmetteur(CompteEmetteur);
        virmenet.setCompteBeneficiaire(CompteBeneficiaire);
        virmenet.setDateExecution(virementDto.getDate());
        return virmenet;
    }
}
