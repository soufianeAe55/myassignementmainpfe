package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;



public interface VersmenetMapper {
    Versement versementRequestToVersement(VersementDto versementDto);
}
