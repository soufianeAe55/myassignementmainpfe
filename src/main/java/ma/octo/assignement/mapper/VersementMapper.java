package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import org.springframework.stereotype.Service;


public class VersementMapper {

    private static VersementDto versementDto;
    private static Versement versement;

    public static VersementDto VersementMap(Versement versement) {
        versementDto = new VersementDto();
        versementDto.setMontant(versement.getMontant());
        versementDto.setNomEmetteur(versement.getNomEmetteur());
        versementDto.setDate(versement.getDateExecution());
        versementDto.setMotif(versement.getMotif());
        return versementDto;

    }
    public static Versement VersementDtoMap(VersementDto versementDto, Compte CompteBeneficiaire ) {
        versement= new Versement();
        versement.setMontant(versementDto.getMontant());
        versement.setMotif(versementDto.getMotif());
        versement.setNomEmetteur(versementDto.getNomEmetteur());
        versement.setCompteBeneficiaire(CompteBeneficiaire);
        versement.setDateExecution(versementDto.getDate());
        return versement;
    }
}
