package ma.octo.assignement.dto;

import lombok.Data;
import ma.octo.assignement.domain.Transaction;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class VirementDto extends TransactionDto {
  private String nrCompteEmetteur;
}
