package ma.octo.assignement.domain;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
@Table(name = "VIREMENT")
public class Virement extends Transaction {

  @ManyToOne
  private Compte compteEmetteur;

}
