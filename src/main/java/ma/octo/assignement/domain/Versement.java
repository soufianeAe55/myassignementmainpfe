package ma.octo.assignement.domain;

import lombok.Data;
import org.springframework.beans.factory.annotation.Required;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
@Table(name = "VERSEMENT")
public class Versement extends Transaction {

  @Column
  private String NomEmetteur;
}
