package ma.octo.assignement.domain;

import lombok.Data;
import ma.octo.assignement.domain.util.EventType;

import javax.persistence.*;

@Entity
@Table(name = "AUDIT_VIREMENT")
@Data
public class AuditVirement extends Audit {

}
