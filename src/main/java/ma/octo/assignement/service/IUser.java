package ma.octo.assignement.service;

import ma.octo.assignement.domain.Utilisateur;

import java.util.List;

public interface IUser {
    List<Utilisateur> loadAllUtilisateurs();
}
