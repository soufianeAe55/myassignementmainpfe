package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import org.springframework.stereotype.Service;

import java.util.List;


public interface ICompte {
    List<Compte> loadAllComptes();
}
