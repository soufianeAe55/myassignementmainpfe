package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

@Service
public class VersementService implements IVersement {
    public static final int MONTANT_MAXIMAL = 10000;

    @Autowired
    private VersementRepository VersementRep;
    @Autowired
    private CompteRepository CompteRep;
    @Autowired
    private AutiService monService;

    @Override
    public List<Versement> loadAllVersements() {
        List<Versement> all = VersementRep.findAll();
        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }

    @Transactional
    @Override
    public String createTransaction(VersementDto versementDto) throws CompteNonExistantException, TransactionException {
        Compte Beneficiare=CompteRep.findByNrCompte(versementDto.getNrCompteBeneficiaire());

        if (Beneficiare == null) {
            throw new CompteNonExistantException("Compte Non existant");
        }
        if (versementDto.getMontant().equals(null)) {
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontant().intValue() == 0) {
            throw new TransactionException("Montant vide");
        }else if (versementDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            throw new TransactionException("Montant maximal de virement dépassé");
        }
        if (versementDto.getMotif().length() < 0) {
            throw new TransactionException("Motif vide");
        }
        if (versementDto.getNomEmetteur().length() < 0) {
            throw new TransactionException("Le nom complet d'emetteur est vide");
        }

        Beneficiare.setSolde(new BigDecimal(Beneficiare.getSolde().intValue() + versementDto.getMontant().intValue()));
        CompteRep.save(Beneficiare);

        Versement versement=VersementMapper.VersementDtoMap(versementDto,Beneficiare);
        VersementRep.save(versement);

        monService.auditVersement("Versement par " + versementDto.getNomEmetteur() + " vers " + versementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + versementDto.getMontant()
                .toString());
        return  "VERSEMENT_TRANSACTION_SUCESSED";
    }
}
