package ma.octo.assignement.service;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


public interface IVirement {
    public List<Virement> loadAllVirements();
    public String createTransaction( VirementDto virementDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException;
}
