package ma.octo.assignement.service;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface IVersement {
    public List<Versement> loadAllVersements();
    public String createTransaction( VersementDto versementDto) throws CompteNonExistantException, TransactionException;
}
