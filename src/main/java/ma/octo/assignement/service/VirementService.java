package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.web.VirementController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

@Service
public class VirementService implements IVirement {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(VirementController.class);

    @Autowired
    private CompteRepository CompteRep;
    @Autowired
    private VirementRepository VirementRep;
    @Autowired
    private AutiService monservice;

    @Override
    public List<Virement> loadAllVirements() {
        List<Virement> all = VirementRep.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }

    @Transactional
    @Override
    public String createTransaction(VirementDto virementDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {

        Compte Emetteur = CompteRep.findByNrCompte(virementDto.getNrCompteEmetteur());
        Compte Beneficiare = CompteRep.findByNrCompte(virementDto.getNrCompteBeneficiaire());

        if (Emetteur == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (Beneficiare == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (virementDto.getMontant().equals(null)) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (virementDto.getMontant().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (virementDto.getMontant().intValue() < 10) {
            System.out.println("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (virementDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        if (virementDto.getMotif().length() < 0) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (Emetteur.getSolde().intValue() - virementDto.getMontant().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }



        Emetteur.setSolde(Emetteur.getSolde().subtract(virementDto.getMontant()));
        CompteRep.save(Emetteur);

        Beneficiare.setSolde(new BigDecimal(Beneficiare.getSolde().intValue() + virementDto.getMontant().intValue()));
        CompteRep.save(Beneficiare);
        VirementMapper.VirementDtoMap(virementDto,Emetteur,Beneficiare);
        Virement virement = VirementMapper.VirementDtoMap(virementDto,Emetteur,Beneficiare);

        VirementRep.save(virement);

        monservice.auditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontant()
                .toString());
        return "VAIREMENT_TRANSACTION_SUCESSED";
    }
}
