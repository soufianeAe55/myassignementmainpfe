package ma.octo.assignement.web;

import lombok.Data;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AutiService;
import ma.octo.assignement.service.IVersement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@Data
public class VersementController {

    @Autowired
    private IVersement versementService;

    @GetMapping("VersmentsList")
    public List<Versement> loadAll() {
        return versementService.loadAllVersements();
    }


    @PostMapping("executerVersements")
    @ResponseStatus(HttpStatus.CREATED)
    public String createTransaction(@RequestBody VersementDto versementDto) throws TransactionException, CompteNonExistantException {
       return versementService.createTransaction(versementDto);
    }



}
