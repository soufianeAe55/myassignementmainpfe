package ma.octo.assignement.web;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.VersementService;
import ma.octo.assignement.service.VirementService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

public class VersementControllerTest {

    @Test
    public void createTransaction() throws TransactionException, CompteNonExistantException {
        VersementDto versementDto=new VersementDto();
        versementDto.setMontant(new BigDecimal(6000));
        versementDto.setDate(new Date());
        versementDto.setMotif("Stage PFE 2022");
        versementDto.setNomEmetteur("MOHAMMED");
        versementDto.setNrCompteBeneficiaire("010000B025001000");

        VersementService versementService= Mockito.mock(VersementService.class);
        when(versementService.createTransaction(versementDto)).thenReturn("VERSEMENT_TRANSACTION_SUCESSED");
        assertEquals("VERSEMENT_TRANSACTION_SUCESSED",versementService.createTransaction(versementDto));
    }
}