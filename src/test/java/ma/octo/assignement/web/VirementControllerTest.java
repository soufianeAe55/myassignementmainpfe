package ma.octo.assignement.web;

import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.IVirement;
import ma.octo.assignement.service.VirementService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


public class VirementControllerTest {

    @Test
    public void createTransaction() throws Exception {
        VirementDto virementDto=new VirementDto();
        virementDto.setMontant(new BigDecimal(2000));
        virementDto.setDate(new Date());
        virementDto.setMotif("Stage PFE 2022");
        virementDto.setNrCompteEmetteur("010000A000001000");
        virementDto.setNrCompteBeneficiaire("010000B025001000");

        VirementService virementService= Mockito.mock(VirementService.class);
        when(virementService.createTransaction(virementDto)).thenReturn("VAIREMENT_TRANSACTION_SUCESSED");
        assertEquals("VAIREMENT_TRANSACTION_SUCESSED",virementService.createTransaction(virementDto));

    }
}