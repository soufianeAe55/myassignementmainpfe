package ma.octo.assignement;

import ma.octo.assignement.web.VersementControllerTest;
import ma.octo.assignement.web.VirementControllerTest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AssignementApplicationTests {

	@Test
	void contextLoads() throws Exception {
		VirementControllerTest vr=new VirementControllerTest();
		vr.createTransaction();
		VersementControllerTest versement=new VersementControllerTest();
		versement.createTransaction();
	}

}
